package com.cap.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticeWebAppApplication.class, args);
		//adding this comment for 
	}

}
